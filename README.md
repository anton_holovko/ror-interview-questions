# Базы данных
* В чем разница реляционных и нереляционных БД
* Типы не реляционных БД
* Какие преимуществa и недостатки реляционных БД
* Какие преимуществa и недостатки нереляционных БД

## Реляционные СУБД
* Нормальные формы.
* Привести таблицу к 3-й нормально форме:

  | book_name          | authors                | publisher  | publisher_zip | publisher_city |
  |--------------------|------------------------|------------|---------------|----------------|
  | Eloquent Ruby      | Olsen R., Fernandez O. | Pearson    | 10001         | New York       |

* Денормализация бд. Когда это допустимо.
* Четыре свойства транзакций БД. Требования ACID:
    - `atomicity`
    - `consistency`
    - `isolation`
    - `durability`
* Уровни изолированости транзакций в БД:
    - `read uncommited`
    - `read commited`
    - `repeatable read`
    - `serializable`



# Ruby questions

## Pure ruby

*  What is a class?

*  What is the difference between a class and a module?

*  What is an object?

*  How would you declare and use a constructor in Ruby?

*  How would you create getter and setter methods in Ruby?

*  Describe the difference between class and instance variables?

*  What are the three levels of method access control for classes and what do they signify?

*  What does ‘self’ mean?

*  Explain how (almost) everything is an object in Ruby.

*  Explain what singleton methods are. What is Eigenclass in Ruby?

*  Describe Ruby method lookup path.

*  What is the difference between Proc and lambda?


* What difference between? `super` and `super()`

* How it works and what difference
```ruby
class Polygon
  @@sides = 10
  @perimeter = 100

  class << self
    def sides
      @@sides
    end

    def perimeter
      @perimeter
    end
  end
end

class Triangle < Polygon
  @@sides = 3
  @perimeter = 9
end

Polygon.sides # => 3
Triangle.sides # => 3

Polygon.perimeter # => 100
Triangle.perimeter # => 9
```

## Business Applications

*  What is Rack?

*  Explain the Rack application interface.

*  Write a simple Rack application.

*  How does Rack middleware works?

## Ruby Gems

*  What is RubyGems? How does it work?

*  How can you build your own Ruby gem?

*  Explain the structure of a Ruby gem.

*  Can you give me some examples of your favorite gems besides Ruby on Rails?



# RoR questions

## Autoloading and Reloading Constants

* What difference between ruby and RoR autoload concepts

* How work RoR code reloading?

* Why next code snippet rises error and who fix it? # TODO
```ruby
# app/controllers/applicaiton_controller.rb
class ApplicationController < ActionController::Base
  ...
end

# app/controllers/account/applicaiton_controller.rb
class Account::ApplicationController < ApplicationController
  ...
end

# app/controllers/account/dashboard_controller.rb
class Account::DashboardController < Account::ApplicationController
  ...
end

# app/models/account.rb
class Account < ApplicationRecord
  ...
end

```

## Active Record

* Explain the Active Record pattern.

* What is Object-Relational Mapping?

* Explain the difference between optimistic and pessimistic locking.

## Security

* What is CSRF? How does Rails protect against it?

* Explain what is a sessions mechanism. How does it work?

* Describe cross-site request forgery, cross-site scripting, session hijacking, and session fixation attacks.

* How should you store secure data such as a password?

* What’s the problem with the following controller code? What would be the consequence of leaving this code in a production app? How would you fix it?
```ruby
  class MyController < ApplicationController
    def options
      options = {}
      available_option_keys = [:first_option, :second_option, :third_option]

      all_keys = params.keys.map(&:to_sym)
      set_option_keys = all_keys & available_option_keys

      set_option_keys.each do |key|
        options[key] = params[key]
      end

      options
    end
  end
```

## Refactoring

* What is a code smell?

* Why should you avoid fat controllers?

* Why should you avoid fat models?

* Explain extract Value, Service, Form, View, Query, and Policy Objects techniques.

* TODO: add more questions

## Performance and Bugs

* What are the problems with following worker's code?
```ruby
class VideoAvailableWorker
  include Sidekiq::Worker

  def perform(user_id)
    user = User.find(user_id)
    SendVideoAvailableService.(user)
  end
end

class SendVideoAvailableService
  def self.call(user)
    @user = user
    @user.video.available!
    VideoAvailableMailer.send_email(@user).deliver_now
  end
end
```

* What problem of that code snippet?
```ruby
class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :email, null: false, index: true
      t.timestamps
    end
  end
end

class User < ApplicationRecord
  validates :email, uniqueness: { case_sensitive: false }
end
```

# Practical tasks

* Write an implementation following snippet of code
```ruby
expect(45).to eq(45)
```
`https://repl.it/@kelevro/WhimsicalBewitchedDatamart?folderId=`

# Home work

## Task Definition

* TODO

## Functional Requirements

* TODO

## Technical Requirements

* TODO
